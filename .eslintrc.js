module.exports = {
	plugins: ['react'],
	extends: ['eslint:recommended', 'plugin:react/recommended'],
	parserOptions: {
		ecmaVersion: 'babel-eslint',
		sourceType: 'module',
		ecmaFeatures: {
			jsx: true,
		},
	},
	env: {
		node: true,
	},
	rules: {
		quotes: ['error', 'single', { avoidEscape: true }],
		'comma-dangle': ['error', 'always-multiline'],
	},
	settings: {
		react: {
			version: "16.4.2",
		},
	},
};

